console.log('1. Animal Class\n');
console.log('\nRelease 0\n');
class Animal {
    constructor(name) {
        this._name = name
        this._legs = 4
        this._cold_blooded  = false
    }

    get name() {
        return this._name;
    }

    get legs() {
        return this._legs;
    }

    get cold_blooded() {
        return this._cold_blooded;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


console.log('\nRelease 1\n');

class Ape extends Animal {
    constructor(name) {
        super(name);
        this._legs = 2;
    }

    yell() {
        console.log('Auooo');
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name);
    }

    jump() {
      console.log('hop hop');
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell();
 
var kodok = new Frog("buduk")
kodok.jump();

console.log('2. Function to Class\n');


class NewClock {

    constructor(params) {
        _template = params.template
    }

    render() {
        var date = new Date();
    
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        
        var output = _template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
    
        console.log(output);
    }

    start(){
        this.render();
        this.timer = setInterval(this.render, 1000);
    }

    stop() {
        clearInterval(this.timer);
    }
    
}

var _template  = {template: 'h:m:s'}; 
var clock = new NewClock(_template);
clock.start();  
// clock.stop();
