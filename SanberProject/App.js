import React from 'react';
import { StyleSheet, Button } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import 'react-native-gesture-handler';

import LoginScreen from './LoginScreen';
import HomeScreen from './HomeScreen';
import AboutScreen from './AboutScreen';
import DetailScreen from './DetailScreen';
import Civilizations from './DetailCivilizations';
import Units from './DetailUnits';
import Structures from './DetailStructures';
import Technologies from './DetailTechnologies';

const Tab = createMaterialBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator style={styles.tabNavigation}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="About" component={AboutScreen} />
    </Tab.Navigator>
  );
}
const Stack = createStackNavigator();
export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login" >
          <Stack.Screen name='Login' component={LoginScreen} options={{ headerShown: false }} />
          <Stack.Screen name='Home' component={MyTabs} options={{ headerShown: false }} />
          <Stack.Screen name='Details' component={DetailScreen} />
          <Stack.Screen name='Civilizations' component={Civilizations} />
          <Stack.Screen name='Units' component={Units} />
          <Stack.Screen name='Structures' component={Structures} />
          <Stack.Screen name='Technologies' component={Technologies} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabNavigation: {
    backgroundColor: '#fff',
    fontSize : 20
  },
});
