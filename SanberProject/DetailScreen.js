import React from 'react';
import { View, Text, FlatList, StyleSheet, ActivityIndicator, Image  } from 'react-native';
import Axios from 'axios';
import SkillItem from './SkillItem';
import skillData from './skillData.json';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class DetailScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          data: {},
          isLoading: true,
          isError: false
        };
    }

      // Mount User Method
  componentDidMount() {
    this.getGithubUser()
  }

    //   Get Api Users
    getGithubUser = async () => {
        try {
          const response = await Axios.get(`https://age-of-empires-2-api.herokuapp.com/api/v1/civilizations`)
          this.setState({ isError: false, isLoading: false, data: response.data })
        } catch (error) {
          this.setState({ isLoading: false, isError: true })
        }
      }

  render() {
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    // If data finish load
    return (



      <View style={styles.container}>
        {/* <FlatList
        data={this.state.data.civilizations}
        renderItem={({ item }) =>
          <View style={styles.viewList}>
            <View>
              <Image source={{ uri: `${item.avatar_url}` }} style={styles.Image} />
            </View>
            <View>
              <Text style={styles.textItemLogin}> {item.name}</Text>
              <Text style={styles.textItemUrl}> {item.expansion}</Text>

            </View>
          </View>
        }
        keyExtractor={({ id }, index) => index}
      /> */}
        <View style={styles.profilContainer}>
          <View style={{justifyContent:'center',height:40}}>
          <MaterialCommunityIcons name="face-agent" size={48} color="black" />
          </View>
          <View style={styles.profilDetails}>
            <Text style={styles.profilHai}>Hai,</Text>
            <Text style={styles.profilName}>Richard Roe</Text>
          </View>
        </View>

        <View style={{borderBottomWidth:5,borderBottomColor:'#3EC6FF'}}>
          <Text style={{fontSize:28,color:'#003366'}}>SKILL</Text>
        </View>

        <FlatList 

            data={this.state.data.civilizations}
            renderItem={({ item }) =>
              <View style={styles.viewList}>
                <View>
                  <Image source={{ uri: `${item.avatar_url}` }} style={styles.Image} />
                </View>
                <View>
                  <Text style={styles.textItemLogin}> {item.name}</Text>
                  <Text style={styles.textItemUrl}> {item.expansion}</Text>
    
                </View>
              </View>
            }
            keyExtractor={({ id }, index) => index}
          />

      </View>
    );
  }

};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  header: {
    backgroundColor: 'white',
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  profilContainer: {
    flexDirection: 'row',
    justifyContent:'center'
  },
  profilDetails: {
    paddingHorizontal: 15,
    flex: 1
  },
  profilHai: {
    fontSize: 16
  },
  profilName: {
    flex: 1,
    fontWeight: 'bold',
    color: '#003366'
  },
  boxKategori: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginBottom: 10
  },
  textKategoriContainer: {
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor:'#B4E9FF',
    borderRadius:10
  },
  textKategori: {
    fontWeight: 'bold',
    color:'#003366'
  },
  headerText: {
    color: 'white',
    fontSize: 18,
    padding: 26,
  },
  scrollContainer: {
    flex: 1,
    marginBottom: 100,
  }
});

