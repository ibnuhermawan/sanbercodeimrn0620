import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions } from 'react-native';

import data from './data.json';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { ImageBackground } from 'react-native';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      totalPrice: 0,
    }
  }

  render() {
    
    return (
      <ImageBackground
        source={require('./images/background.png')}
        style={{width: '100%', height: '100%'}}
      >
          <View style={styles.container}></View>
            <FlatList 
              data={data.aoe2}
              renderItem={(data)=> {
                if (data.item.id == '1') {
                  return(
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Civilizations') }>
                      <ListItem data={data.item} />
                    </TouchableOpacity>
                  )     
                }
                if (data.item.id == '2') {
                  return(
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Units') }>
                      <ListItem data={data.item} />
                    </TouchableOpacity>
                  )     
                }

                if (data.item.id == '3') {
                  return(
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Structures') }>
                      <ListItem data={data.item} />
                    </TouchableOpacity>
                  )     
                }
                if (data.item.id == '4') {
                  return(
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Technologies') }>
                      <ListItem data={data.item} />
                    </TouchableOpacity>
                  )     
                }
               }
              }
              keyExtractor={(data)=>data.id}
              ItemSeparatorComponent={()=><View style={{height:10}} />}
            />
          
      </ImageBackground>



      // <View style={styles.container}>
      //   <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>

      //   <FlatList 
      //         data={data.aoe2}
      //         renderItem={(data)=> {
      //           if (data.item.id == '1') {
      //             return(
      //               <TouchableOpacity onPress={() => this.props.navigation.navigate('Civilizations') }>
      //                 <ListItem data={data.item} />
      //               </TouchableOpacity>
      //             )     
      //           }
      //           if (data.item.id == '2') {
      //             return(
      //               <TouchableOpacity onPress={() => this.props.navigation.navigate('Units') }>
      //                 <ListItem data={data.item} />
      //               </TouchableOpacity>
      //             )     
      //           }

      //           if (data.item.id == '3') {
      //             return(
      //               <TouchableOpacity onPress={() => this.props.navigation.navigate('Structures') }>
      //                 <ListItem data={data.item} />
      //               </TouchableOpacity>
      //             )     
      //           }
      //           if (data.item.id == '4') {
      //             return(
      //               <TouchableOpacity onPress={() => this.props.navigation.navigate('Technologies') }>
      //                 <ListItem data={data.item} />
      //               </TouchableOpacity>
      //             )     
      //           }
      //          }
      //         }
      //         keyExtractor={(data)=>data.id}
      //         ItemSeparatorComponent={()=><View style={{height:10}} />}
      //       />
      //   </View>
      // </View>
    )
  }
};

class ListItem extends React.Component {

  render() {
    const data = this.props.data
    return (
      <View style={styles.itemContainer} key={this.props.keyval}>
        <View style={{justifyContent:'center'}}>
          <Image source={{ uri: data.imageUri }} style={styles.itemImage} />
				</View>
        <View style={styles.infoDetails}>
          <Text ellipsizeMode='tail' style={styles.itemName} >{data.name}</Text>
          <Icon name="chevron-right" size={DEVICE.width * 0.20} color="#003366" />
				</View>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  headerText: {
    fontSize: 12,
    fontWeight: 'bold'
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    flexDirection: 'row',
		paddingTop: 5,
		paddingLeft: 5,
		paddingBottom: 5,
		backgroundColor: '#e9e0c1',
		borderRadius: 10,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 5,
		},
		shadowOpacity: 0.36,
		shadowRadius: 6.68,

		elevation: 11,    
  },
  infoDetails: {
		paddingHorizontal: 15,
	},
  itemImage: {
    width: DEVICE.width * 0.30, height: DEVICE.width * 0.30
  },
  itemName: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
		color: '#8b0000',
  },
  itemButton: {
  },
  buttonText: {
  }
})
