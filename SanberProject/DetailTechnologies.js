import React from 'react';
import { View, Text, FlatList, StyleSheet, ActivityIndicator, Image  } from 'react-native';
import Axios from 'axios';
import SkillItem from './SkillItem';
import skillData from './skillData.json';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class DetailScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          data: {},
          isLoading: true,
          isError: false
        };
    }

      // Mount User Method
  componentDidMount() {
    this.getGithubUser()
  }

    //   Get Api Users
    getGithubUser = async () => {
        try {
          const response = await Axios.get(`https://age-of-empires-2-api.herokuapp.com/api/v1/technologies`)
          this.setState({ isError: false, isLoading: false, data: response.data })
        } catch (error) {
          this.setState({ isLoading: false, isError: true })
        }
      }

  render() {
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    // If data finish load
    return (



      <View style={styles.container}>
        
        <View style={styles.profilContainer}>
          <View style={{justifyContent:'center',height:40}}>

          </View>
          <View style={styles.profilDetails}>
            <Text style={styles.profilHai}>Technology is the method of advancing a player's civilization in the Age of Empires series. Once researched, technologies grant permanent (with very few exceptions) improvement. Technologies are usually grouped into a few categories.</Text>
          </View>
        </View>

        <View style={{borderBottomWidth:2,borderBottomColor:'#DDD'}}>
        
        </View>

        <FlatList 
            data={this.state.data.technologies}
            renderItem={({ item }) =>
              <View style={styles.viewList}>
                <View>
                  <Image source={{ uri: `${item.avatar_url}` }} style={styles.Image} />
                </View>
                <View style={{borderBottomWidth:2,borderBottomColor:'#DDD', paddingBottom:10, paddingTop:10}}>
                  <Text style={styles.textItemLogin}><Text style={styles.profilName}>Name :</Text> {item.name}</Text>
                  <Text><Text style={styles.profilName}>Description :</Text> {item.description}</Text>
                  <Text><Text style={styles.profilName}>Expansion :</Text> {item.expansion}</Text>
                  <Text><Text style={styles.profilName}>Age :</Text> {item.age}</Text>
                  <Text><Text style={styles.profilName}>Build Time :</Text> {item.build_time}</Text>
                </View>
              </View>
            }
            keyExtractor={({ id }, index) => index}
          />

      </View>
    );
  }

};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  header: {
    backgroundColor: 'white',
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  profilContainer: {
    flexDirection: 'row',
    justifyContent:'center'
  },
  profilDetails: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    flex: 1
  },
  profilHai: {
    fontSize: 16
  },
  profilName: {
    flex: 1,
    fontWeight: 'bold',
    color: '#003366'
  },
  boxKategori: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginBottom: 10
  },
  textKategoriContainer: {
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor:'#B4E9FF',
    borderRadius:10
  },
  textKategori: {
    fontWeight: 'bold',
    color:'#003366'
  },
  headerText: {
    color: 'white',
    fontSize: 18,
    padding: 26,
  },
  scrollContainer: {
    flex: 1,
    marginBottom: 100,
  }
});

