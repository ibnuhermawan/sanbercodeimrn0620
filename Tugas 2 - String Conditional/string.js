
console.log('');
console.log('=== A. TUGAS STRING ===');
console.log('--- NOMOR 1 ---'); 
//JavaScript is awesome and I love it! 
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';
var SPACE = " ";

var result = word.concat(SPACE, second, SPACE, third, SPACE, fourth, SPACE, fifth, SPACE, sixth, SPACE, seventh);
console.log(result);
console.log('');
console.log('--- NOMOR 2 ---');
// First word: I 
// Second word: am 
// Third word: going 
// Fourth word: to 
// Fifth word: be 
// Sixth word: React 
// Seventh word: Developer 

var sentence = "I am going to be React Native Developer"; 
var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]; 

var arrSentence = sentence.split(" "); // tidak jadi, ga ada dimateri.

var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var fourthWord = sentence[11] + sentence[12];
var fiftWord = sentence[14] + sentence[15];
var sixthWord = sentence[17] + sentence[18]+ sentence[19] + sentence[20]+sentence[21];
var seventhWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fiftWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 

console.log('');
console.log('--- NOMOR 3 ---');
// First Word: wow 
// Second Word: JavaScript 
// Third Word: is 
// Fourth Word: so 
// Fifth Word: cool 
var sentence2 = 'wow JavaScript is so cool'; 
var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14);  
var thirdWord2 = sentence2.substring(15, 17);  
var fourthWord2 = sentence2.substring(18, 20);  
var fifthWord2 = sentence2.substring(21, 25);  
console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);


console.log('');
console.log('--- NOMOR 4 ---');
// First Word: wow, with length: 3 
// Second Word: JavaScript, with length: 10 
// Third Word: is, with length: 2 
// Fourth Word: so, with length: 2 
// Fifth Word: cool, with length: 4
var sentence3 = 'wow JavaScript is so cool'; 
var exampleFirstWord3 = sentence3.substring(0, 3);
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3 = sentence3.substring(18, 20);
var fifthWord3 = sentence3.substring(21, 25);

var firstWordLength = exampleFirstWord3.length  
var secondWordLength = secondWord3.length  
var thirdWordLength = thirdWord3.length  
var fourthWordLength = fourthWord3.length  
var fifthWordLength = fifthWord3.length  

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength);  
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength);  
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength);  
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength);  

console.log('');
console.log('=== B. TUGAS CONDITIONAL ===');
console.log('--- NOMOR 1 ---');

var nama = "Ibnu";
var peran = "penyihir";

if (nama.length < 1) {
    console.log('Nama harus diisi!');
} else if (peran.length < 1) {
    console.log('Halo '+nama+', Pilih peranmu untuk memulai game!');
}else{
    var aliasPeran = peran.toLowerCase();
    switch(aliasPeran) {
        case 'penyihir':   { 
            msg = "Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!";
            break; 
        }
        case 'guard':   { 
            msg = "Halo Guard "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.";
            break;
         }
        case 'werewolf':   { 
            msg = "Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!" 
            break; 
        }
        default:  { 
            msg = "Peran kamu tidak diketahui" 
    }}
    console.log('Selamat datang di Dunia Werewolf, '+nama);
    console.log(msg);
}


console.log('');
console.log('--- NOMOR 2 ---');
var hari = '5'; 
var bulan = 5; 
var tahun = 2050;

if (isNaN(hari) || isNaN(bulan) || isNaN(tahun)) {
    console.log('Mohon masukkan angka');
}else{
    hari = parseInt(hari);
    bulan = parseInt(bulan);
    tahun = parseInt(tahun);
    if ((hari >= 1 && hari <= 31) && (bulan >= 1 && bulan <=12) && (tahun >= 1900 && tahun <=2200)) {
        var bulanAlias = '';
        switch(parseInt(bulan)) {
        case 1:   { bulanAlias = 'Januari'; break; }
        case 2:   { bulanAlias = 'Februari'; break; }
        case 3:   { bulanAlias = 'Maret'; break; }
        case 4:   { bulanAlias = 'April'; break; }
        case 5:   { bulanAlias = 'Mei'; break; }
        case 6:   { bulanAlias = 'Juni'; break; }
        case 7:   { bulanAlias = 'Juli'; break; }
        case 8:   { bulanAlias = 'Agustus'; break; }
        case 9:   { bulanAlias = 'September'; break; }
        case 10:   { bulanAlias = 'Oktober'; break; }
        case 11:   { bulanAlias = 'November'; break; }
        case 12:   { bulanAlias = 'Desember'; break; }
        default:  { bulanAlias = 'Tidak Diketahui'; }}
        
        var fulldate = hari+SPACE+bulanAlias+SPACE+tahun;
        console.log(fulldate);
    } else {
        console.log('Masukkan angka dengan benar, untuk Hari antara 1 - 31, Bulan angka antara 1 - 12, Tahun antara 1900 - 2200 ');
    }
    
}


