
console.log("=== SOAL NO. 1 (RANGE) ===\n");

function range(start,finish) {
	if (start > 0 && finish > 0) {
		var array = [];
		var temp = start;
		if (start < finish) {
			while (temp <= finish) {
				array.push(temp);
				temp++;
			}
		} else {
			while (temp >= finish) {
				array.push(temp);
				temp--;
			}
		}
		return array;
	} else {
		return -1;
	}
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


console.log("\n=== SOAL NO. 2 (RANGE WITH STEP) ===\n");

function rangeWithStep(start,finish,step) {
	var array = [];
	if (start > 0 && finish > 0) {
		var array = [];
		var temp = start;
		if (start < finish) {
			while (temp <= finish) {
				array.push(temp);
				temp = temp+step;
			}
		} else {
			while (temp >= finish) {
				array.push(temp);
				temp = temp-step;
			}
		}
	}
	return array;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


console.log("\n=== SOAL NO. 3 (SUM OF RANGE) ===\n");

function sum(start=0,finish=1,step=1) {
	var array = rangeWithStep(start,finish,step);
	var jumlah = 0;
	for (var i = 0; i < array.length; i++) {
		jumlah = jumlah + array[i];
	}
	return jumlah;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 



console.log("\n=== SOAL NO. 4 (ARRAY MULTIDIMENSI) ===\n");

var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ]

function dataHandling(input) {
	var text = '';
	if (input.length > 0) {
		for (var i = 0; i < input.length; i++) {
			text += "Nomor ID: "+input[i][0]+"\n"
			text += "Nama Lengkap: "+input[i][1]+"\n"
			text += "TTL: "+input[i][2]+" "+input[i][3]+"\n"
			text += "Hobi: "+input[i][4]+"\n\n"
		}
	}
	return text;
}
console.log(dataHandling(input));


console.log("\n=== SOAL NO. 5 (BALIK KATA) ===\n");

function balikKata(text) {
    var array = [];
    var test = '';
	for (var i = text.length-1; i >= 0; i--) {
        test = test + text[i];
	}
    return test;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


console.log("\n=== SOAL NO. 6 (METODE ARRAY) ===\n");

function dataHandling2(params) {
    
    params.splice(1,4,params[1].trim()+" Elsharawy","Provinsi "+params[2],params[3],"Pria","SMA Internasional Metro");
    
	console.log(params);
    var bulan = params[3].split("/");
    var name_bulan = '';
	switch(bulan[1]){
		case '01':
			name_bulan = "Januari"
		break;
		case '02':
			name_bulan = "Februari"
		break;
		case '03':
			name_bulan = "Maret"
		break;
		case '04':
			name_bulan = "April"
		break;
		case '05':
			name_bulan = "Mei"
		break;
		case '06':
			name_bulan = "Juni"
		break;
		case '07':
			name_bulan = "Juli"
		break;
		case '08':
			name_bulan = "Agustus"
		break;
		case '09':
			name_bulan = "September"
		break;
		case '10':
			name_bulan = "Oktober"
		break;
		case '11':
			name_bulan = "November"
		break;
		case '12':
			name_bulan = "Desember"
		break;
	}
	console.log(name_bulan);
	var split = bulan.slice();
	bulan.sort(function(value1, value2){return value2-value1});
	console.log(bulan);
	var text_strip = split.join('-');
	console.log(text_strip);
	var text_nama = String(params[1]).slice(0,15);
	console.log(text_nama);
}

var params = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(params);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 
console.log("\n\n\n\n")


function test(){
	const propkey = 'field 12';
	const person = {
		[propkey] : 'abduh'
	};
	console.log(person['field 12']);
}

test();


function test1(){
	let myOtherName;
	const myName = 'abduh';
	myOtherName = 'abduh';
	console.log(myName);
}

test1();

function test2(){
	const daftarHobi = ['olahraga','makan','ngoding'];
	let hobiFavorit;
	for (const hobi of daftarHobi) {
		hobiFavorit = hobi;
	}
	console.log(hobiFavorit);
}

test2();


function test3(){
	const enterval = 'test';
	const username = enterval && 'adas';

	console.log(username);
}

test3();


function test4(){
	var result = (2+3===7 &&4>5||'hi'==='hi')

	console.log(result);
}

test4();