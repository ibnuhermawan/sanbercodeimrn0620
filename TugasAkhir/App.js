import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View, Text, TextInput, StyleSheet, Button } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

// import 'react-native-gesture-handler';

import LoginScreen from './LoginScreen';
import HomeScreen from './HomeScreen';
import DetailScreen from './DetailScreen';

const Stack = createStackNavigator();
const Tabs = createMaterialBottomTabNavigator();
export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login" >
          <Stack.Screen name='Login' component={LoginScreen} options={{ headerShown: false }} />
          <Stack.Screen name='Tabs' component={TabScreen} options={{ headerShown: false }} />
          <Stack.Screen name='Details' component={DetailScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const TabScreen = ({navigation}) => {
  return (
    <Tabs.Navigator>
      <Tabs.Screen name="Home" component={HomeScreen} options={{ headerTitle: 'AoE II Information' }} navigation={{navigation}} />
      <Tabs.Screen name="Setting" component={HomeScreen} />
    </Tabs.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
