var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
function rekursip(waktu,obj_buku,i=0){
	if (obj_buku.length > i) {
		readBooks(waktu,obj_buku[i], function(sisa_waktu){
			if (sisa_waktu > 0) {rekursip(sisa_waktu,obj_buku,i+1)}
		})
	}
}
rekursip(10000,books);