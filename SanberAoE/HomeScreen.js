import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions } from 'react-native';

import data from './data.json';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import reduxStore from './store';
import { ImageBackground } from 'react-native';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      apiUri: '',
      caption: '',
    }
  }

  render() {
    let varTodos = reduxStore.getState('todos');
    console.log(varTodos[0]);
    return (
      <ImageBackground
        source={require('./images/background.png')}
        style={{width: '100%', height: '100%'}}
      >
      <View style={styles.container}>
        <View>
          <Text>{this.props.todos}</Text>
        </View>
        {/* <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}> */}
        <FlatList 
              data={data.aoe2}
              renderItem={(data)=> {
                return(
                    <ListItem data={data.item} navObj={this.props.navigation}/>
                )
               }
              }
              keyExtractor={(data)=>data.id.toString()}
              ItemSeparatorComponent={()=><View style={{height:10}} />}
            />
        </View>
      {/* </View> */}
      </ImageBackground>
    )
  }
};

class ListItem extends React.Component {

  render() {
    const itemData = this.props.data
    return (
      <TouchableOpacity onPress={() => this.props.navObj.navigate('Details', {itemData}) }>
      <View style={styles.itemContainer} key={this.props.keyval}>
        <View style={{justifyContent:'center'}}>
          <Image source={{ uri: itemData.imageUri }} style={styles.itemImage} />
				</View>
        <View style={styles.infoDetails}>
          <Text ellipsizeMode='tail' style={styles.itemName} >{itemData.name}</Text>
          <Icon name="chevron-right" size={DEVICE.width * 0.20} color="#003366" />
				</View>
      </View>
      </TouchableOpacity>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
  headerText: {
    fontSize: 12,
    fontWeight: 'bold'
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    flexDirection: 'row',
		paddingTop: 5,
		paddingLeft: 5,
		paddingBottom: 5,
		backgroundColor: '#e9e0c1',
		borderRadius: 10,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 5,
		},
		shadowOpacity: 0.36,
		shadowRadius: 6.68,

		elevation: 11,    
  },
  infoDetails: {
		paddingHorizontal: 15,
	},
  itemImage: {
    width: DEVICE.width * 0.30, height: DEVICE.width * 0.30
  },
  itemName: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
		color: '#8b0000',
  },
  itemButton: {
  },
  buttonText: {
  }
})
