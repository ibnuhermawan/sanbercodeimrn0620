import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Image,
    Text
} from 'react-native'

import { MaterialIcons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
export default class App extends Component {

    state = {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }

    render() {

        return (
            <View style={styles.container}>
                <Image source={require('./images/logo.png')} style={styles.backgroundImage} />
                <View style={styles.body}>
                    <View style={{alignItems:'center'}}>
                        <Text style={styles.text}>
                            Developer
                        </Text>
                    </View>
                    <View style={{alignItems:'center'}}>
                        <MaterialIcons name="face" size={120} color="red" />
                    </View>
                    <View style={{alignItems:'center'}}>
                        {/* <Text style={[styles.textName, styles.label]}>
                            Dody Naftali 
                        </Text> */}
                        <Text style={[styles.textProfile, styles.label]}>
                            Dody Naftali - Ibnu Hermawan
                        </Text>
                    </View>
					<View style={styles.boxPortfolio}>
						<Text>Portfolio:</Text>
                        <View style={{ borderBottomColor: 'black', borderBottomWidth: 1, }}/>
                        <View style={styles.portfolio, {flexDirection: 'row', justifyContent: 'space-between'}}>
                            <View style={{flexDirection: 'column'}}>
                                <MaterialCommunityIcons name="github-circle" size={42} color="blue" />
                                <Text style={[styles.label]}>
                                dnaftali
                                </Text>
                            </View>
                            <View style={{flexDirection: 'column'}}>
                                <MaterialCommunityIcons name="github-circle" size={42} color="blue" />
                                <Text style={[styles.label]}>
                                ibnuhermawan
                                </Text>       
                            </View>
                        </View>
					</View> 
                    <View>
                        <Text></Text>
                    </View>         
  
                    <View>
                        <Text></Text>
                    </View>                     
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor : 'black'
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
      },
    layout: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.05)',
    },
    box: {
        padding: 25,
        backgroundColor: 'steelblue',
        margin: 5,
    },
    text: {
        fontSize: 36,
        color: '#8b0000',
        fontWeight: 'bold'
    },
    textName: {
        fontSize: 24,
        color: '#8b0000',
        fontWeight: 'bold'
    },
    textProfile: {
        fontSize: 18,
        color: '#8b0000',
        fontWeight: 'bold',
        fontStyle: "italic"
    },
    boxPortfolio: {
        padding: 10,
        fontSize: 18,
        color: '#3EC6FF',
        fontWeight: 'bold',
        backgroundColor: '#e9e0c1',
        borderRadius:15,
        justifyContent: 'space-around',
        flexDirection: 'column'
    },
    portfolio: {
        flexDirection: 'column',
        margin: 5,
        alignItems:'center'
    },
    label: {
        padding: 4,
        color: 'red'
    },
    body: {
        paddingTop: 0,
    }
})