import React from 'react';
import { StyleSheet, Button } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

import { Provider } from 'react-redux'

import 'react-native-gesture-handler';

import reduxStore from './store'
import LoginScreen from './LoginScreen';
import HomeScreen from './HomeScreen';
import AboutScreen from './AboutScreen';
import DetailScreen from './DetailScreen';

const Tab = createMaterialBottomTabNavigator();

function MyTabs({navigation}) {
  return (
    <Tab.Navigator barStyle={{ backgroundColor: '#8b0000' }}>
      <Tab.Screen name="HomeScreen" component={HomeScreen} navigation={navigation} />
      <Tab.Screen name="About" component={AboutScreen} />
    </Tab.Navigator>
  );
}
const Stack = createStackNavigator();
export default class App extends React.Component {
  render() {
    return (
      <Provider store={reduxStore}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="Login" >
            <Stack.Screen name='Login' component={LoginScreen} options={{ headerShown: false }} />
            <Stack.Screen name='Home' component={MyTabs} options={{ headerShown: false }} />
            <Stack.Screen name='Details' component={DetailScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
