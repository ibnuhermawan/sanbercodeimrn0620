console.log('Soal No. 1 (Array to Object)\n');
function arrayToObject(params) {
    
    if ( params.length == 0) {
        console.log('Parameter tidak valid');
    }

    var listIdentitas = [];
    var identitas = {};
    for (let i = 0; i < params.length; i++) {
        var jumlahMaxArrObj = params[i].length;
        var lastIndex = params[i].length - 1;

        // firstName
        if (typeof params[i][0] != 'undefined') {
            identitas.firstName = params[i][0];
        }

        // lastName
        if (typeof params[i][1] != 'undefined') {
            identitas.lastName = params[i][1];
        }

        // gender
        if (typeof params[i][2] != 'undefined') {
            identitas.gender = params[i][2];

            // check index ke 3 tidak ada.
            if (jumlahMaxArrObj-1 == lastIndex) {
                identitas.age = "Invalid Birth Year";
            }
        }

        // age
        if (typeof params[i][3] != 'undefined') {
            var umur = getUmur(params[i][3]);
            if (umur < 0) {
                umur = "Invalid Birth Year";
            }
            identitas.age = umur;
        }else{
            identitas.age =  "Invalid Birth Year";
        }

        // Jika sudah index terakhir --> print!
        if (jumlahMaxArrObj-1 == lastIndex) {
            var result = i+1 + '. '+identitas.firstName +' '+ identitas.lastName;
            listIdentitas.push(identitas);
            console.log(result, identitas);
            identitas = {};
        }
        
    }
}

function getUmur(paramsTahunLahir){
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)

    return thisYear - paramsTahunLahir;
}


var params = [["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985]];
// var params = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]];
// var params = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]];
arrayToObject(params);


console.log('\nSoal No. 2 (Shopping Time)\n');

function shoppingTime(memberId, money){

    var produk = [{nama:'Sepatu Stacattu', harga: 1500000},
    {nama:'Baju Zoro', harga: 500000},
    {nama:'Baju H&N', harga: 250000},
    {nama:'Sweater Uniklooh', harga: 175000},
    {nama:'Casing Handphone', harga: 50000}];
    
    if (memberId.length < 1) {
        console.log('Mohon maaf, toko X hanya berlaku untuk member saja');
    }else{
        if (parseInt(money) < 50000) {
            console.log('Mohon maaf, uang tidak cukup');
        }else{
            var listProduk = produk.slice();
            listProduk.sort(function(a,b) {
                return b.harga - a.harga;
            });

            var pembelian = [];
            var jumlahUang = money;

            for( var i=0; i<listProduk.length;i++){
                if( jumlahUang >= listProduk[i].harga ){
                  pembelian.push(listProduk[i].nama);
                  jumlahUang = (jumlahUang-listProduk[i].harga);
                }
              }
        
              var output = {memberId:memberId, money:money,listPurchased:pembelian,changeMoney:jumlahUang}
              return output;
        }
    }
}

console.log(shoppingTime('324193hDew2', 700000));

console.log('\nSoal No. 3 (Naik Angkot)\n');
function naikAngkot(arrPenumpang) {

    var result = [];

    if (arrPenumpang.length < 1) {
        return result;
    }
    
    var ongkosRute = 2000;
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    
    var start = 0;
    var finish = 0;
    for (let index = 0; index < rute.length; index++) {
        if (rute[index] == arrPenumpang[0][1]) {
            start = index;
        }  
        
        if (rute[index] == arrPenumpang[0][2]) {
            finish = index;
        }
    }

    
    var obj = {};
    obj.penumpang = arrPenumpang[0][0];
    obj.naikDari = arrPenumpang[0][1];
    obj.tujuan = arrPenumpang[0][2];
    obj.bayar = (finish - start) * ongkosRute;
    result.push(obj);
    return result;
  }

var params =  [['Dimitri', 'B', 'F'] ];
console.log(naikAngkot(params));
console.log(naikAngkot([]));
// output: [{ penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 }];