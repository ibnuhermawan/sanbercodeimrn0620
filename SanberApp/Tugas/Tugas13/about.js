import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Image,
    Text
} from 'react-native'

import { MaterialIcons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class App extends Component {

    state = {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }

    render() {

        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <View style={{alignItems:'center'}}>
                        <Text style={[styles.textTitle,styles.label]}>
                            Tentang BukaLapuk
                        </Text>
                    </View>
                    <View style={{ borderBottomColor: 'black', borderBottomWidth: 2, }}/>
                    <View style={{alignItems:'center'}}>
                        <Text style={[styles.textDesc,styles.label]}>
                            BukaLapuk adalah perusahaan penyedia informasi lowongan pekerjaan terkemuka di Sanber. 
                        </Text>
                    </View>
                    <View style={styles.boxSubTitle}>
                        <Text style={[styles.textSubTitle, styles.label]}>
                            Developer
                        </Text>
					</View>
                    <View style={{alignItems:'center'}}>
                        <MaterialCommunityIcons name="baby-face" size={150} color="black" />
                    </View>
                    <View style={{alignItems:'center'}}>
                        <Text style={[styles.textName, styles.label]}>
                            Ibnu Hermawan
                        </Text>
                        <Text style={[styles.textProfile, styles.label]}>
                            React Native Developer
                        </Text>
                    </View>
                    <View style={{alignItems:'center'}}>
                        <Text style={[styles.textDesc,styles.label]}>
                        </Text>
                    </View>
					<View style={styles.boxPortfolio}>
						<Text>PORTOFOLIO</Text>
                        <View style={{ borderBottomColor: 'black', borderBottomWidth: 1, }}/>
                        <View style={styles.portfolio}>
                            {/* <View style={{flexDirection: 'row'}}> */}
                                <MaterialCommunityIcons name="gitlab" size={42} color="#3EC6FF" />
                                <Text style={[styles.label]}>
                                    ibnuhermawan
                                </Text> 
                                <MaterialCommunityIcons name="github-face" size={42} color="#3EC6FF" />
                                <Text style={[styles.label]}>
                                    ibnuhermawan
                                </Text>                            
                            {/* </View> */}
                            </View>                 
                        </View>
                        <View style={styles.portfolio}>
					</View>                    
					<View style={styles.boxPortfolio}>
						<Text>PROFIL</Text>
                        <View style={{ borderBottomColor: 'black', borderBottomWidth: 1, }}/>
                        <View style={styles.profile}>
                                <View style={{flexDirection: 'row'}}>
                                    <MaterialCommunityIcons name="instagram" size={42} color="#3EC6FF" />                       
                                    <Text style={[styles.label]}>
                                        @ibnuhermawan
                                    </Text>
                                </View>
                                <View style={{flexDirection: 'row'}}>
                                    <MaterialCommunityIcons name="facebook" size={42} color="#3EC6FF" />
                                    <Text style={[styles.label]}>
                                        @ibnuhermawan
                                    </Text>
                                </View>
                                <View style={{flexDirection: 'row'}}>
                                    <MaterialCommunityIcons name="twitter-box" size={42} color="#3EC6FF" />                          
                                    <Text style={[styles.label]}>
                                        @ibnuhermawan
                                    </Text>
                                </View>
                                
                        </View>
					</View>                    
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    layout: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.05)',
    },
    box: {
        padding: 25,
        backgroundColor: 'steelblue',
        margin: 5,
    },
    text: {
        fontSize: 36,
        color: '#003366',
        fontWeight: 'bold'
    },
    textDesc: {
        fontSize: 18,
        color: '#003366',
        fontWeight: 'normal'
    },
    textTitle: {
        fontSize: 36,
        color: '#003366',
        fontWeight: 'bold'
    },
    textSubTitle: {
        fontSize: 24,
        color: '#003366',
        fontWeight: 'bold',
    },
    textName: {
        fontSize: 24,
        color: '#003366',
        fontWeight: 'bold'
    },
    textProfile: {
        fontSize: 18,
        color: '#3EC6FF',
        fontWeight: 'bold',
        fontStyle: "italic"
    },
    boxPortfolio: {
        padding: 10,
        fontSize: 18,
        color: '#3EC6FF',
        fontWeight: 'bold',
        backgroundColor: '#EFEFEF',
    },
    boxSubTitle: {
        padding: 5,
        borderStyle  : "solid",
        fontSize: 18,
        borderColor : "black",
        color: '#fff',
        fontWeight: 'bold',
        backgroundColor: '#F0DEDE',
    },
    portfolio: {
        flexDirection: 'row',
        margin: 5,
        alignItems:'center'
    },
    profile: {
        flexDirection: 'column',
        margin: 5,
        alignItems:'center'
    },
    label: {
        padding: 4,
        // color: '#003366'
    },
    body: {
        paddingTop: 50,
    }
})